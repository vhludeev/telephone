DROP DATABASE IF EXISTS `telephone`;

CREATE DATABASE `telephone` DEFAULT CHARACTER SET utf8 ;

CREATE  TABLE `telephone`.`records` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `last_name` VARCHAR(45) NOT NULL ,
  `first_name` VARCHAR(45) NOT NULL ,
  `middle_name` VARCHAR(45) NOT NULL ,
  `street_id` INT NOT NULL ,
  `birth_date` DATE NOT NULL ,
  `phone_number` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE  TABLE `telephone`.`streets` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL ,
  `city_id` INT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE  TABLE `telephone`.`cities` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

ALTER TABLE `telephone`.`streets`
  ADD CONSTRAINT `cityKey`
  FOREIGN KEY (`city_id` )
  REFERENCES `telephone`.`streets` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `cityKey_idx` (`city_id` ASC) ;

ALTER TABLE `telephone`.`records`
  ADD CONSTRAINT `streetKey`
  FOREIGN KEY (`street_id` )
  REFERENCES `telephone`.`streets` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `streetKey_idx` (`street_id` ASC);

INSERT INTO `telephone`.cities(id, name)
    VALUES (1, 'Москва'), (2, 'Хабаровск'), (3, 'Владивосток');
INSERT INTO `telephone`.streets(id, name, city_id)
    VALUES (1, 'Новый Арбат',1);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (2, 'Тверская',1);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (3, 'Пионерская',1);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (4, 'Ленина',2);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (5, 'Восточное шоссе',2);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (6, 'Пушкина',2);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (7, 'Светланская',3);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (8, 'Луговая',3);
INSERT INTO `telephone`.streets(id, name, city_id)
  VALUES (9, 'Русская',3);
INSERT INTO `telephone`.records(last_name, first_name, middle_name, street_id, birth_date, phone_number)
    VALUES ('Иванов', 'Сергей', 'Владимирович', 1, '1977-11-05', '+79468978822');
INSERT INTO `telephone`.records(last_name, first_name, middle_name, street_id, birth_date, phone_number)
    VALUES ('Алексеев', 'Николай', 'Павлович', 8, '1988-04-17', '+79024856629')
