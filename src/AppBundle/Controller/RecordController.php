<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Record;
use AppBundle\Entity\City;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\Street;

class RecordController extends Controller {

    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct() {
        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array("city"));
        $normalizers = array($normalizer);
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @Route("/save_or_update")
     */
    public function saveOrUpdate(Request $request) {
        $req = $request->request;
        $errors = array();
        $id = $req->get("id");
        $firstName = $req->get("firstName");
        $lastName = $req->get("lastName");
        $middleName = $req->get("middleName");
        $phoneNumber = $req->get("phoneNumber");
        $streetId = $req->get("streetId");
        $birthDateString = $req->get("birthDate");
        $errors = $this->validateLength($errors, $firstName, "firstName");
        $errors = $this->validateLength($errors, $lastName, "lastName");
        $errors = $this->validateLength($errors, $middleName, "middleName");
        $errors = $this->validateLength($errors, $phoneNumber, "phoneNumber");
        $errors = $this->validateLength($errors, $streetId, "streetId");
        $birthDate = date_create_from_format('d.m.Y', $birthDateString);
        if(!$birthDate) {
            $errors["birthDate"] = "Дата указана неверно";
        }
        $street = $this->getDoctrine()->getRepository('AppBundle:Street')->find($streetId); /** @var $street Street*/
        if($street == null) {
            $errors["streetId"] = "Улица не выбрана";
        }
        if(!empty($errors)) {
            $errors["hasErrors"] = true;
            $serializedErrors = $this->serializer->serialize($errors, 'json');
            $response = new Response($serializedErrors);
            return $response;
        }
        $record = $this->getDoctrine()->getRepository('AppBundle:Record')->find($id); /** @var $record Record */
        if($record == null) {
            $record = new Record();
        }
        $record->setFirstName($firstName);
        $record->setLastName($lastName);
        $record->setMiddleName($middleName);
        $record->setPhoneNumber($phoneNumber);
        $record->setBirthDate($birthDate);
        $record->setStreet($street);
        $em = $this->getDoctrine()->getManager();
        $em->persist($record);
        $em->flush();
        return new Response("{}");
    }

    private function validateLength(array $errors, $parameter, $parameterName) {
        if($parameter == null || trim($parameter) == "") {
            $errors[$parameterName] = "Поле не заполнено";
        }
        else if(strlen($parameter) > 45) {
            $errors[$parameterName] = "Длина превышает 45 символов";
        }
        return $errors;
    }

    /**
     * @Route("/", name="list")
     */
    public function getList() {
        $records = $this->getDoctrine()->getRepository('AppBundle:Record')
            ->findBy(
                array(),
                array(
                    'lastName' => 'ASC',
                    'firstName' => 'ASC',
                    'middleName' => 'ASC'
                )
            );
        return $this->render('default/record.list.html.twig', array(
            'records' => $records,
        ));
    }

    /**
     * @Route("/edit/{recordId}")
     */
    public function getEditPage($recordId) {

        $record = $this->getDoctrine()->getRepository('AppBundle:Record')->find($recordId);
        if($record == null) {
            return $this->render('default/record.notFound.html.twig');
        }

        return $this->render('default/record.edit.html.twig', array(
            'record' => $record,
            'cities' => $this->getDoctrine()->getRepository('AppBundle:City')->findAll(),
        ));
    }

    /**
     * @Route("/delete/{recordId}")
     */
    public function delete($recordId) {

        $record = $this->getDoctrine()->getRepository('AppBundle:Record')->find($recordId);
        if($record == null) {
            return $this->render('default/record.notFound.html.twig');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($record);
        $em->flush();
        return $this->redirectToRoute('list');
    }

    /**
     * @Route("/add")
     */
    public function getAddPage() {
        return $this->render('default/record.edit.html.twig', array(
            'record' => new Record(),
            'cities' => $this->getDoctrine()->getRepository('AppBundle:City')->findAll(),
        ));
    }

    /**
     * @Route("/streets/{cityId}")
     */
    public function findStreetsByCity($cityId) {
        $city = $this->getDoctrine()->getRepository('AppBundle:City')->find($cityId); /** @var $city City */
        $streets = array();
        if($city != null) {
            $streets = $city->getStreets();
        }
        $serializedStreets = $this->serializer->serialize($streets, 'json');
        $response = new Response($serializedStreets);
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
        return $response;
    }
}
