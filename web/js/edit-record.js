$(function(){
    $("#cityId").on("change", function() {
        var streetSelect = $("#streetId");
        streetSelect.html('<option value="0">Загрузка...</option>'); // сбрасываем список сразу, т.к. загрузка может быть долгой
        $.ajax({
                url: "/streets/" + this.value,
                dataType: 'json',
                success: function(data) {
                    var selectHtml = '<option value="0">Выберите улицу</option>';
                    for (var x = 0; x < data.length; x++) {
                        selectHtml += '<option value="'+ data[x].id +'">'+ data[x].name +'</option>';
                    }
                    streetSelect.html(selectHtml);
                }
            }
        );
    });
});

$(function(){
    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        language: 'ru'
    });
});

$(function(){
	$('#submit-btn').on("click", function(event) {
		var form = $('#edit-form');
		var data = form.serializeArray();
		$.ajax({
			url: "/save_or_update",
			type: "POST",
			data: data,
			success: function(responseText) {
				var response = JSON.parse(responseText);
				var helpBlockSelector = ".help-block";

				function showError(inputTagName, parameterToValidate, errorMessage) {
					var inputSelector = inputTagName + "[name=" + parameterToValidate + "]";
					var formGroup = form.find(inputSelector).closest('.form-group');
					if (errorMessage != null) {
						formGroup.addClass('has-error');
						formGroup.find(helpBlockSelector).removeClass("hidden");
						formGroup.find(helpBlockSelector).html(errorMessage);
					}
					else {
						formGroup.removeClass('has-error');
						formGroup.addClass('has-success');

						formGroup.find(helpBlockSelector).addClass("hidden");
					}
				}

				if(response.hasErrors) {
					showError("input", "lastName", response.lastName);
					showError("input", "firstName", response.firstName);
					showError("input", "middleName", response.middleName);
					showError("input", "phoneNumber", response.phoneNumber);
					showError("input", "birthDate", response.birthDate);
					showError("select", "streetId", response.streetId);
				}
				else {
					window.location.replace("/");
				}
			}
		});
	})
});